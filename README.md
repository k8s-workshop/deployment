# Implantação de Projetos


O Kubernetes nos oferece algumas possibilidades sobre implantação de projetos, que visam garantir a disponilidade (redução de downtime) e a diminuição de impacto na publição de atualizações ou novos projetos em produção.

Vamos verificar a estrutura básica de serviço e pod:


![Defautl Backend](imagens/service-pod.png)


Vamos utilizar o namespaces "nginx-ingress" para dar continuidade nos cenários.

primeiramente, remova a aplicação "cars-api" (vamos fazer o deploy de outra versão):

```
# kubectl delete deploy cars-api -n nginx-ingress
# kubectl delete service cars-api -n nginx-ingress
```

`deployment.extensions "cars-api" deleted`
`service "cars-api" deleted`

Liste os recursos do namespace "nginx-ingress":

```
kubectl get all -n nginx-ingress
NAME                                            READY   STATUS    RESTARTS   AGE
pod/default-http-backend-55b84578bf-bxb8f       1/1     Running   0          83m
pod/nginx-ingress-controller-7479c8c494-544fp   1/1     Running   0          83m

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)                 AGE
service/default-http-backend   NodePort       10.101.161.226   <none>         80:21469/TCP            42h
service/nginx-ingress          LoadBalancer   10.109.25.194    35.229.55.68   80:80/TCP,443:443/TCP   40h

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/default-http-backend       1/1     1            1           42h
deployment.apps/nginx-ingress-controller   1/1     1            1           40h

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/default-http-backend-55b84578bf       1         1         1       42h
replicaset.apps/nginx-ingress-controller-7479c8c494   1         1         1       40h
```

Estratégias de deploy:


# Highlander  

![Defautl Backend](imagens/highlander.png) 


Execute os comandos abaixo:

```
kubectl apply -f cars-api-v1-deployment.yaml -n nginx-ingress
kubectl apply -f cars-api-v1-service.yaml -n nginx-ingress
```

`
 deployment.extensions/cars-api-v1 created
 service/cars-api created
`

Verifique os recursos criados:

```
kubectl get all -n nginx-ingress
NAME                                            READY   STATUS    RESTARTS   AGE
pod/cars-api-v1-5c8558b4b5-95kx5                1/1     Running   0          3m6s
pod/cars-api-v1-5c8558b4b5-fnj9b                1/1     Running   0          3m6s
pod/default-http-backend-55b84578bf-rcxpn       1/1     Running   0          130m
pod/nginx-ingress-controller-7479c8c494-8cw6x   1/1     Running   0          130m

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)                 AGE
service/cars-api               NodePort       10.100.190.227   <none>         8004:2570/TCP           91s
service/default-http-backend   NodePort       10.101.161.226   <none>         80:21469/TCP            7d19h
service/nginx-ingress          LoadBalancer   10.109.25.194    35.229.55.68   80:80/TCP,443:443/TCP   7d17h

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cars-api-v1                2/2     2            2           3m6s
deployment.apps/default-http-backend       1/1     1            1           7d19h
deployment.apps/nginx-ingress-controller   1/1     1            1           7d17h

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/cars-api-v1-5c8558b4b5                2         2         2       3m6s
replicaset.apps/default-http-backend-55b84578bf       1         1         1       7d19h
replicaset.apps/nginx-ingress-controller-7479c8c494   1         1         1       7d17h
```


Acesse no browser o recurso criado:

http://IP_EXTERNO:NODE_PORT/cars/home

Você verá a versão 1 executando.


Abra outra aba e conecte no servidor remoto. Como root, faça:

watch kubectl get all -n nginx-ingress

O comando watch atualiza de 2 em 2 segundos os recursos.

Na outra aba, vamos lançar a nova versão do projeto:

```
kubectl apply -f cars-api-v2-deployment.yaml -n nginx-ingress
```

Volte para a aba onde estamos exdecutando o comando "watch" e veja pods com status "terminating" e outros com status "ContainerCreating"


Acesse no browser o recurso criado:

http://IP_EXTERNO:NODE_PORT/cars/home

Você verá a versão 2 executando.



Delete os recursos criados nesta seção, com o kubectl delete...e vamos para a próxima!


# Rollout

![Defautl Backend](imagens/rollout.png) 


Entre no diretório "rollout" e liste os arquivos:

```
cars-api-v1-deployment.yaml  cars-api-v1-service.yaml  cars-api-v2-deployment.yaml
```

Execute os comandos abaixo:

```
kubectl apply -f cars-api-v1-deployment.yaml -n nginx-ingress
kubectl apply -f cars-api-v1-service.yaml -n nginx-ingress
```

`
 deployment.extensions/cars-api-v1 created
 service/cars-api created
`

Verifique os recursos criados:

```
kubectl get all -n nginx-ingress
```



Acesse no browser o recurso criado:

http://IP_EXTERNO:NODE_PORT/cars/home

Você verá a versão 1 executando.


Abra outra aba e conecte no servidor remoto. Como root, faça:

watch kubectl get all -n nginx-ingress

O comando watch atualiza de 2 em 2 segundos os recursos.

Na outra aba, vamos lançar a nova versão do projeto:

```
kubectl apply -f cars-api-v2-deployment.yaml -n nginx-ingress
```

Volte para a aba onde estamos exdecutando o comando "watch" e veja pods...


Acesse no browser o recurso criado:

http://IP_EXTERNO:NODE_PORT/cars/home

Você verá a versão 2 executando.



Delete os recursos criados nesta seção, com o kubectl delete...e vamos para a próxima!


# Blue / Green 

Cenário:

Digamos que temos a nova versão de produção como sendo a versão 1 (blue), que nos responde através do Ingress.

Como fazemos para lançar uma nova versão, testar em piloto e depois transformar em produção?


![Defautl Backend](imagens/blue-green.png)




Entre no diretório "blue-green" do nosso projeto git.

teremos 4 arquivos: cars-api-v1-deployment.yaml  cars-api-v1-service.yaml  cars-api-v2-deployment.yaml  cars-api-v2-service.yaml

Vamos aplicar os 4 arquivos:

```
root@node1:/home/leandro/blue-green# 

kubectl apply -f . -n nginx-ingress
```

`
 deployment.extensions/cars-api-v1 created
 service/cars-api created
 deployment.extensions/cars-api-v2 
 created service/cars-api-service-v2 created
`

Vejamos o resultado:

```
kubectl get all -n nginx-ingress

NAME                                            READY   STATUS    RESTARTS   AGE
pod/cars-api-v1-5f76f59496-qnw4x                1/1     Running   0          2m12s
pod/cars-api-v2-56c85c44d7-2bf6p                1/1     Running   0          2m12s
pod/default-http-backend-55b84578bf-bxb8f       1/1     Running   0          87m
pod/nginx-ingress-controller-7479c8c494-544fp   1/1     Running   0          87m

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)                 AGE
service/cars-api               ClusterIP      10.104.64.5      <none>         8004/TCP                2m12s
service/cars-api-service-v2    NodePort       10.100.14.254    <none>         8004:6695/TCP           2m12s
service/default-http-backend   NodePort       10.101.161.226   <none>         80:21469/TCP            42h
service/nginx-ingress          LoadBalancer   10.109.25.194    35.229.55.68   80:80/TCP,443:443/TCP   40h

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cars-api-v1                1/1     1            1           2m12s
deployment.apps/cars-api-v2                1/1     1            1           2m12s
deployment.apps/default-http-backend       1/1     1            1           42h
deployment.apps/nginx-ingress-controller   1/1     1            1           40h

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/cars-api-v1-5f76f59496                1         1         1       2m12s
replicaset.apps/cars-api-v2-56c85c44d7                1         1         1       2m12s
replicaset.apps/default-http-backend-55b84578bf       1         1         1       42h
replicaset.apps/nginx-ingress-controller-7479c8c494   1         1         1       40h
```

Temos dois serviços / deploys criados:

cars-api / cars-api-v1

cars-api-service-v2 / cars-api-v2 


Acesse a versão 1 do serviço:

http://IP_EXTERNO/cars/list

Acesse a versão 2 do serviço:

http://IP_EXTERNO:NODE_PORT/cars/list


Vamos mudar o seletor do serviço versão 1 para apontar para o deploy da versão2:

![Defautl Backend](imagens/blue-green-producao.png)


Execute:

```
kubectl edit svc cars-api -n nginx-ingress
```

altere:

```
selector:
    app: cars-api-v1
```

Para:

```
selector:
    app: cars-api-v2
```


Salve e feche o arquivo.


Acesse:

http://IP_EXTERNO/cars/home


E veja que agora temos uma nova versão!

![Defautl Backend](imagens/versao2.png)


# Canary 

![Defautl Backend](imagens/canary1.png)

![Defautl Backend](imagens/canary2.png)


Execute os comandos abaixo:

```
kubectl apply -f cars-api-v1-deployment.yaml -n nginx-ingress
kubectl apply -f cars-api-v1-service.yaml -n nginx-ingress
```

`
 deployment.extensions/cars-api-v1 created
 service/cars-api created
`

Verifique os recursos criados.


Acesse no browser o recurso criado:

http://IP_EXTERNO:NODE_PORT/cars/home

Você verá a versão 1 executando.


Agora vamos criar a versão 2 da nossa aplicação:

```
kubectl apply -f cars-api-v2-deployment.yaml -n nginx-ingress
```
```
kubectl get all -n nginx-ingress
NAME                                            READY   STATUS    RESTARTS   AGE
pod/cars-api-v1-75c9f7f9d5-hxws4                1/1     Running   0          173m
pod/cars-api-v2-6795b58746-gmzd9                1/1     Running   0          172m
pod/cars-api-v2-6795b58746-kcflc                1/1     Running   0          172m
pod/default-http-backend-55b84578bf-rcxpn       1/1     Running   0          24h
pod/nginx-ingress-controller-7479c8c494-8cw6x   1/1     Running   0          24h

NAME                           TYPE           CLUSTER-IP       EXTERNAL-IP    PORT(S)                 AGE
service/cars-api               NodePort       10.100.246.151   <none>         8004:20427/TCP          175m
service/default-http-backend   NodePort       10.101.161.226   <none>         80:21469/TCP            8d
service/nginx-ingress          LoadBalancer   10.109.25.194    35.229.55.68   80:80/TCP,443:443/TCP   8d

NAME                                       READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/cars-api-v1                1/1     1            1           173m
deployment.apps/cars-api-v2                2/2     2            2           172m
deployment.apps/default-http-backend       1/1     1            1           8d
deployment.apps/nginx-ingress-controller   1/1     1            1           8d

NAME                                                  DESIRED   CURRENT   READY   AGE
replicaset.apps/cars-api-v1-75c9f7f9d5                1         1         1       173m
replicaset.apps/cars-api-v2-6795b58746                2         2         2       172m
replicaset.apps/default-http-backend-55b84578bf       1         1         1       8d
replicaset.apps/nginx-ingress-controller-7479c8c494   1         1         1       8d
```


Observe que temos: cars-api-v1 (1 réplica) e cars-api-v2 (2 réplica).

Ao acessar a aplicação no browser, você pode ser redirecionado tanto para a aplicação 1, quanto para a aplicação 2.

Se a versão 2 for bem sucedida, basta remover as réplicas da versão anterior!!!

